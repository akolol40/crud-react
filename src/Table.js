import React, {Component} from 'react';
import axios from 'axios'

import Modal from 'react-modal'

class Table extends Component  
{

  constructor(props) {
        super(props);

        this.state = {newvalue: '',value: '', showModal: false, text: "Edit", data:[], np: '', id: 0};


        this.handleOpenModal = this.handleOpenModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleOpenModal () {
        this.setState({ showModal: true });
      }
      
      handleCloseModal () {
        this.setState({ showModal: false });
      }
    handleChange(event) {
        this.setState({newvalue: event.target.value});
      }
    
      handleSubmit(event) {
        event.preventDefault();
    }
    render()
    {

    const DeleteUsername = id =>
    {
      axios.delete('http://178.128.196.163:3000/api/records/'+id)
      .then(res => {
        if (res.data == true)
        {
          this.props.DeleteUser(id);
        } else alert("invalid connect server, check your internet connection")
      })
    }



   const UpdateUsername = (id,name) =>
    {
        const obj = {
            _id: id,
            data: {name},
            __v: 0};    
          axios.post("http://178.128.196.163:3000/api/records/"+id, obj).then(res=> 
          {const data = res.data;
            let _name=data.data.name;
            console.log(_name);
           if (_name!=""){
           this.props.Change();
           this.state.newvalue="";
           this.handleCloseModal();
           }});
    }

    const GetMyUser = id => {
      fetch('http://178.128.196.163:3000/api/records/'+id)
      .then(response => response.json())
      .then(data => this.setState({data})).then(
      this.handleOpenModal());
    }

    const GetName = data => 
    {
      this.state.np = data;
      try
      {
        this.state.np = data.name;
        this.state.value = data.name;
      }
      catch(err)
      {
        console.log("nonepars")
      }
    }

    const GetId = data => 
    {
      this.state.id = data;
      try
      {
        this.state.id = data._id
      }
      catch(err)
      {
        console.log("nonepars")
      }
    }


    return (
          <table className="table">
              <thead>
                 <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Actions</th>
                 </tr>
              </thead>
            
             <tbody>
               
                {this.props.data.map(item => (
                     <tr key={item._id}>
                         <td>{item._id}</td>  
                         <td>{item.data.name}</td>  
                         <td> 
                             <button className="button Delete-button" onClick={() => DeleteUsername(item._id)}> Delete</button> 
                             {" "}
                             <button onClick={()=>GetMyUser(item._id)}>Edit</button>
                                <Modal 
                                isOpen={this.state.showModal}
                                contentLabel="Edit User"
                                >
                                
                                {GetName(this.state.data.data)}
                                {GetId(this.state.data)}
                                <form onSubmit={this.handleSubmit}>
                                  <label>Last Name: </label><input type="text" name="name"  value={this.state.value}  onChange={this.handleChange}/>
                                    <label>New Name: </label><input type="text" name="name"  value={this.state.newvalue}  onChange={this.handleChange}/>
                                    <button onClick={() => UpdateUsername(this.state.id,this.state.newvalue)}>Save changes</button>
                                    {"  "}
                                    <button onClick={this.handleCloseModal}>Close</button>
                                </form>
                                </Modal>
                            
                        </td>
                     </tr>
                ))}
             </tbody>

          </table>
        );
}
}

export default Table;