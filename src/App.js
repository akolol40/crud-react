import React, { Component } from 'react';
import './App.css';
import Table from './Table'
import AddUser from './AddUser'



class App extends Component {
  constructor(props)
  {
    super(props);

    this.state = {
      data: [],
    }
  }

  componentDidMount()
  {
     fetch('http://178.128.196.163:3000/api/records')
    .then(response => response.json())
    .then(data => this.setState({data}));
  }


  
  render() {
    
      const DeleteUser = _id => 
      {
        this.setState(prevState => ({
          data: prevState.data.filter(el => el._id != _id)
        }));
      };

      const add=(_id,Name) =>
      {
        this.state.data.push({_id: _id, data:{name: Name}}); 
        this.setState({data: this.state.data});  
      } 

      const ChangeName = () =>
      {
        fetch('http://178.128.196.163:3000/api/records')
        .then(response => response.json())
        .then(data => this.setState({data}));
      }


    return (
      <div className="App">
        <Table data={this.state.data} Change={ChangeName} DeleteUser={DeleteUser}/>
         <AddUser AddUser={add} />
      </div>
      
    );
  }
}

export default App;
