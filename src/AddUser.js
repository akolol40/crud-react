import React, {Component} from 'react';

class AddUser extends Component
{
    constructor(props) {
        super(props);
        this.state = {value: ''};
    
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }
    
      handleChange(event) {
        this.setState({value: event.target.value});
      }
    
      handleSubmit(event) {
        event.preventDefault();
      }

      

      

    render()
    {
        const AddNewUser = Name => {
            fetch("http://178.128.196.163:3000/api/records/",{
                method: "PUT",
                body: JSON.stringify({data: {name: Name}}),
                headers: {
                'content-type': 'application/json'
                }
            }).then(response => response.json())
            .then(data => {if (data!="") {this.props.AddUser(data._id, Name)} else {alert("error")}});
              this.state.value='';
              console.log();
            };
            

        return(
            <form onSubmit={this.handleSubmit}>
            <input type="text" name="name"  value={this.state.value}  onChange={this.handleChange}/>
            <button onClick={() => AddNewUser(this.state.value)}>Add new user</button>
            </form>
        )
    }
    
}

export default AddUser;